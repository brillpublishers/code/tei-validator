FROM python:3-alpine
WORKDIR /lib
RUN apk add --update --no-cache gcc musl-dev libxml2-dev libxslt-dev
COPY . ./
RUN pip install --extra-index-url="https://alpine-wheels.github.io/index" "$(pwd)"
WORKDIR /app
ENTRYPOINT ["tei-validator"]
