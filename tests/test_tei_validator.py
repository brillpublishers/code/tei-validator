from lxml.etree import RelaxNG

from tei_validator import load_validator, validate_file


def test_load_validator():
    validator = load_validator()
    assert isinstance(validator, RelaxNG)


def test_non_wellformed_xml():
    errors = validate_file("tests/fixtures/non_wellformed.xml")
    assert errors, "Expected invalid xml to produce errors"


def test_no_tei_xml():
    errors = validate_file("tests/fixtures/no_tei.xml")
    assert not errors, "Expected no errors from non-TEI xml"


def test_valid_tei():
    errors = validate_file("tests/fixtures/valid_tei_all.xml")
    assert not errors, "Expected no errors from valid TEI xml"
